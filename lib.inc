section .text

%define SYS_EXIT 60
%define STDOUT 1
%define SYS_WRITE 1
%define SYS_READ 0
%define STDIN 0
%define CHAR_NEWLINE 0xA
%define CHAR_ZERO '0'

 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, SYS_EXIT
    xor  rdi, rdi          
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, SYS_WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE         ; Sys_write code(1)  
    mov rdi, STDOUT            ; Stdout descriptor(1)    
    mov rsi, rsp               ; Put char link
    mov rdx, 1                 ; Char size in bytes(1)
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, CHAR_NEWLINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, 10          	; Base num system
	mov rax, rdi		    ; Put parametr in rax	    
	xor rcx, rcx		    ; Clear symbols counter
	
	dec rsp			
	inc rcx
	mov byte [rsp], 0
.loop:
	xor rdx, rdx        	; Clear rdx
	div r9              	; Divide on 10
	add rdx, CHAR_ZERO      ; Transfer to ascii symbol
	dec rsp
	inc rcx
	mov byte [rsp], dl   	; Put symbol in buffer
	test rax, rax       	; Check the end of operation
	jnz .loop		        ; Return if not zero
	
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor rdx, rdx
	cmp rdi, 0		    ; Check if num have sign
	jns .print_unsign	; Print if not have
	push rdi		    ; Save rdi value
	mov rdi, '-'
	call print_char		; Print minus char
	pop rdi			
	neg rdi			    ; Take unsign form of rdi
    .print_unsign:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  push rbx
  xor rbx, rbx
    xor rax, rax
  .loop:
    mov cl, byte [rdi+rbx]
    cmp cl, byte [rsi+rbx]
    jnz .not_ok
    cmp byte [rdi + rbx], 0
    jz .ok
    inc rbx
    jmp .loop
  .ok:
    mov rax, 1
    jmp .end
  .not_ok:
    mov rax, 0
    jmp .end
  .end:
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0			; 0 if end of stream
	mov rdx, 1		; Count of symbols(1)
	mov rsi, rsp	; Char link in stack
	mov rdi, STDIN 	; Stdin descriptor(0)
	xor rax, rax	; Sys_read code(0)
	syscall
	pop rax			; Take rax from stack
 	ret 
    

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    .read_spaces:
     call read_char
        cmp al, 0x20
        je .read_spaces
        cmp al, 0x9
        je .read_spaces
        cmp al, 0xA
        je .read_spaces
    xor rdx, rdx
    .loop:
        cmp al, 0xA
        je .finish
        cmp al, 0x20
        je .finish
        cmp al, 4
        je .finish
        cmp al, 0x9
        je .finish
        cmp al, 0
        je .finish
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        push rdx
        push r8
        call read_char
        pop r8
        pop rdx
        jmp .loop
    .finish:
        mov byte [r8+rdx], 0
        mov rax, r8
        ret
    .overflow:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается (rdi)
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor r8, r8
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi
    mov r9, 10
    .loop:
        mov sil, [rdi+r8]
        cmp sil, '0'
        jl .finish
        cmp sil, '9'
        jg .finish
        inc r8
        sub sil, '0'
        mul r9
        add rax, rsi
        jmp .loop
    .finish:
        mov rdx, r8
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	cmp byte [rdi], '-'	
	je .ngt	
	jmp parse_uint		
    .ngt:
        inc rdi
        push rdi
        call parse_uint	
        pop rdi	
        neg rax			
        inc rdx			
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
	push rdi
	push rsi
	push rdx		    ; Save caller-saved registers
	call string_length	
	pop rdx
	pop rsi
	pop rdi			    ; Restore caller-saved registers
	inc rax			    ; Null-terminator space
	cmp rax, rdx		; Compare string and buffer length
	ja .finish		
	xor rcx, rcx		; Clear rcx
    .loop:
        mov dl, byte [rdi]	
        mov byte [rsi], dl	; Move symbol from string to buffer
        inc rdi
        inc rsi			    ; Increase pointers
        inc rcx		    	; Increase counter
        cmp rcx, rax		; Check buffer status
        jna .loop		    ; If not filled then continue
        dec rax			    ; String length without nul-terminator
        ret
    .finish:
        mov rax, 0		    ; Put 0 in rax
        ret